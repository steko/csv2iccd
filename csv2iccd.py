#! /usr/bin/env python3
# Copyright 2019 Stefano Costa <steko@iosa.it>

import argparse
import logging
import sys
import tempfile
import time

import xml.etree.ElementTree as ET

from datetime import date
from zipfile import ZipFile


def export_zip(a_s, esc, tsk, ver, xml_tree):
    '''Export a zipfile ready for import in SIGEC.'''
    fn_trc = '{}{}{}'.format(a_s, esc, tsk)

    with ZipFile('{}_{}_{}_{:.0f}.zip'.format(tsk, ver, esc, time.time()), 'w') as z:
        with tempfile.TemporaryDirectory() as tmpdirname:
            # pacchetto dati
            with open('{}/{}.xml'.format(tmpdirname, fn_trc), 'wb') as g:
                xml_tree.write(
                    g,
                    encoding='UTF-8',
                    xml_declaration=True,
                    short_empty_elements=False)
            z.write('{}/{}.xml'.format(tmpdirname, fn_trc), '{}.xml'.format(fn_trc))
            # geoInfo
            with open('{}/geoInfo.xml'.format(tmpdirname), 'w') as g:
                g.write('<?xml version="1.0" encoding="UTF-8"?>')
            z.write('{}/geoInfo.xml'.format(tmpdirname), 'geoInfo.xml')
            # INFORMA
            with open('{}/INFORMA.xml'.format(tmpdirname), 'w') as g:
                g.write('<?xml version="1.0" encoding="UTF-8"?>')
            z.write('{}/INFORMA.xml'.format(tmpdirname), 'INFORMA.xml')


def main():
    '''Convert from CSV file to ZIP archive with XML content.

    The CSV file must have columns corresponding to the ICCD field names.
    '''
    if sys.version_info.major == 2:
        sys.exit('Please run this script with Python 3')

    from csv import DictReader
    parser = argparse.ArgumentParser(description='convert CSV data to ICCD serialisation format')
    parser.add_argument(
        'input', metavar='input', type=argparse.FileType('r', encoding='utf-8'),
        help='input CSV data file')
    parser.add_argument('--esc', help='Ente schedatore')
    parser.add_argument('--ver', help='Versione normativa')
    parser.add_argument('--tsk', help='Tipo scheda')
    args = parser.parse_args()

    with args.input as csvfile:
        data = DictReader(csvfile)
        records = [row for row in data]
        numrec = len(records)
        a_s = 'A' if args.tsk == 'BIB' else 'S'
        x = xml_skeleton(args.tsk, args.ver, args.esc, numrec)
        r = xml_content(x, records)
        tree = ET.ElementTree(r)
        export_zip(a_s, args.esc, args.tsk, args.ver, tree)


def xml_skeleton(tsk, ver, esc, numrec):
    csm_root = ET.Element('csm_root')
    csm_info = ET.SubElement(csm_root, 'csm_info')
    nome_normativa = ET.SubElement(csm_info, 'nome_normativa')
    nome_normativa.text = tsk
    tipo_normativa = ET.SubElement(csm_info, 'tipo')
    tipo_normativa.text = 'altre normative'
    ver_numero = ET.SubElement(csm_info, 'ver_numero')
    ver_numero.text = ver
    data_crea = ET.SubElement(csm_info, 'data_crea')
    data_crea.text = '{:%d%m%Y}'.format(date.today())
    ente_schedatore = ET.SubElement(csm_info, 'ente_schedatore')
    ente_schedatore.text = esc
    concessione = ET.SubElement(csm_info, 'concessione')
    spedizione = ET.SubElement(csm_info, 'spedizione')
    note = ET.SubElement(csm_info, 'note')
    numero_schede = ET.SubElement(csm_info, 'numero_schede')
    numero_schede.text = '{:08d}'.format(numrec)

    return csm_root
    # tree.write('tree.xml', encoding='UTF-8', xml_declaration=True)


def xml_content(xml_tree, records):
    schede = ET.SubElement(xml_tree, 'schede')
    for r in records:
        scheda = ET.SubElement(schede, 'scheda')
        # CODICI
        cd = ET.SubElement(scheda, 'CD')
        tsk = ET.SubElement(cd, 'TSK')
        tsk.text = 'MINP'  # FIXME
        cdm = ET.SubElement(cd, 'CDM')
        cdm.text = r['CDM']
        esc = ET.SubElement(cd, 'ESC')
        esc.text = 'S236'  # FIXME
        ecp = ET.SubElement(cd, 'ECP')
        ecp.text = r['ECP']
        cre = ET.SubElement(cd, 'CRE')
        cre.text = r['CRE']
        cuf = ET.SubElement(cd, 'CUF')
        cuf.text = r['CUF']
        # OGGETTO
        og = ET.SubElement(scheda, 'OG')
        amb = ET.SubElement(og, 'AMB')
        amb.text = r['AMB']
        ogd = ET.SubElement(og, 'OGD')
        ogd.text = r['OGD']
        ctg = ET.SubElement(og, 'CTG')
        ctg.text = r['CTG']
        # LOCALIZZAZIONE FISICA
        lc = ET.SubElement(scheda, 'LC')
        pvc = ET.SubElement(lc, 'PVC')
        pvcr = ET.SubElement(pvc, 'PVCR')
        pvcr.text = r['PVCR']
        pvcp = ET.SubElement(pvc, 'PVCP')
        pvcp.text = r['PVCP']
        pvcc = ET.SubElement(pvc, 'PVCC')
        pvcc.text = r['PVCC']
        ldc = ET.SubElement(lc, 'LDC')
        ldcn = ET.SubElement(ldc, 'LDCN')
        ldcn.text = r['LDCN']
        ldcu = ET.SubElement(ldc, 'LDCU')
        ldcu.text = r['LDCU']
        ldcs = ET.SubElement(ldc, 'LDCS')
        ldcs.text = r['LDCS']
        # MATERIA E TECNICA
        mt = ET.SubElement(scheda, 'MT')
        qnt = ET.SubElement(mt, 'QNT')
        qntc = ET.SubElement(qnt, 'QNTC')
        qntc.text = r['QNTC']
        qntn = ET.SubElement(qnt, 'QNTN')
        qntn.text = r['QNTN']
        mis = ET.SubElement(mt, 'MIS')
        misz = ET.SubElement(mis, 'MISZ')
        misz.text = r['MISZ']
        misu = ET.SubElement(mis, 'MISU')
        misu.text = r['MISU']
        mism = ET.SubElement(mis, 'MISM')
        mism.text = r['MISM']
        # DATAZIONE
        dt = ET.SubElement(scheda, 'DT')
        dtr = ET.SubElement(dt, 'DTR')
        dtr.text = r['DTR']
        # REPERIMENTO
        re = ET.SubElement(scheda, 'RE')
        dsc = ET.SubElement(re, 'DSC')
        dscj = ET.SubElement(dsc, 'DSCJ')
        dscj.text = r['DSCJ']
        dsch = ET.SubElement(dsc, 'DSCH')
        dsch.text = r['DSCH']
        dscv = ET.SubElement(dsc, 'DSCV')
        dscv.text = r['DSCV']
        dscd = ET.SubElement(dsc, 'DSCD')
        dscd.text = r['DSCD']
        dscf = ET.SubElement(dsc, 'DSCF')
        dscf.text = r['DSCF']
        dsca = ET.SubElement(dsc, 'DSCA')
        dsca.text = r['DSCA']
        # DATI PATRIMONIALI
        ub = ET.SubElement(scheda, 'UB')
        inp = ET.SubElement(ub, 'INP')
        inpf = ET.SubElement(inp, 'INPF')
        inpf.text = r['INPF']
        inpc = ET.SubElement(inp, 'INPC')
        inpc.text = r['INPC']
        inpe = ET.SubElement(inp, 'INPE')
        inpe.text = r['INPE']
        inpr = ET.SubElement(inp, 'INPR')
        inpr.text = r['INPR']
        inpd = ET.SubElement(inp, 'INPD')
        inpd.text = r['INPD']
        inpp = ET.SubElement(inp, 'INPP')
        inpp.text = r['INPP']
        inpm = ET.SubElement(inp, 'INPM')
        inpm.text = r['INPM']
        inpa = ET.SubElement(inp, 'INPA')
        inpa.text = r['INPA']
        inpz = ET.SubElement(inp, 'INPZ')
        inpz.text = r['INPZ']
        inps = ET.SubElement(inp, 'INPS')
        inps.text = r['INPS']
        inpu = ET.SubElement(inp, 'INPU')
        inpu.text = r['INPU']
        # CONSERVAZIONE
        co = ET.SubElement(scheda, 'CO')
        stc = ET.SubElement(co, 'STC')
        stcc = ET.SubElement(stc, 'STCC')
        stcc.text = r['STCC']
        # STATO GIURIDICO
        tu = ET.SubElement(scheda, 'TU')
        cdg = ET.SubElement(tu, 'CDG')
        cdgg = ET.SubElement(cdg, 'CDGG')
        cdgg.text = r['CDGG']
        # DOCUMENTAZIONE
        do = ET.SubElement(scheda, 'DO')
        fta = ET.SubElement(do, 'FTA')
        ftax = ET.SubElement(fta, 'FTAX')
        ftax.text = r['FTAX']
        ftap = ET.SubElement(fta, 'FTAP')
        ftap.text = r['FTAP']
        ftak = ET.SubElement(fta, 'FTAK')
        ftak.text = r['FTAK']
        # COMPILAZIONE
        cm = ET.SubElement(scheda, 'CM')
        cmc = ET.SubElement(cm, 'CMC')
        cmc.text = r['CMC']
        cma = ET.SubElement(cm, 'CMA')
        cma.text = r['CMA']
        adp = ET.SubElement(cm, 'ADP')
        adp.text = r['ADP']

    return xml_tree


if __name__ == '__main__':
    main()
