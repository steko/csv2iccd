'''Conversione di feature poligonali da shapefile al formato ICCD.'''

import argparse
import random

import fiona


### normativa 3.00 SOLO CAMPO GEOREFERENZIAZIONE

parser = argparse.ArgumentParser(description='convert SHP data to ICCD serialisation format')
parser.add_argument(
    'input', metavar='input', help='input Shapefile')
args = parser.parse_args()

with fiona.open(args.input) as src:
    for feature in src:
        if True:
            res = ''
#            res += 'CD:\nTSK:  CF\nLIR:  I\n'
#            ccf = '{:0>13}'.format(random.randint(1,10e12))
#            res += 'CCF:  ICCD_CF_{}\n'.format(ccf)
#            res += 'ESC:  S19\n'
            res += 'GA:\n'
            res += 'GAL:  localizzazione fisica\n'
            res += 'GAD:\n'
            for xy in feature['geometry']['coordinates']:
                for x, y in xy:
                    res += 'GADP:\n'
                    res += 'GADPX: {}\n'.format(x)
                    res += 'GADPY: {}\n'.format(y)
            res += 'GAM:  perimetrazione esatta\n'
            res += 'GAT:  rilievo da cartografia senza sopralluogo\n'
            res += 'GAP:  WGS84\n'

            #res += 'GPT:  rilievo tramite punti di appoggio fiduciali o trigonometrici\nGPM:  posizionamento esatto\n'
#            res += '''AD:
#ADS:
#ADSP: 1
#ADSM: scheda contenente dati liberamente accessibili
#CM:
#CMP:
#CMPD: 2016
#CMPN: Costa, Stefano
#FUR:  Barbaro, Barbara'''
            res.replace('\n\n', '\n')
            print(res)
        else:
            pass

## normativa 4.00 SCHEDA COMPLETA

header = '''4.00SS19CF          20062016       10000

                                                                                '''

#with fiona.open('Vincoli_Archeologici/Vincoli_Archeologici_WGS84.shp') as src:
#    print(header)
#    for feature in src:
#        if feature['properties']['COD_DOC'] in ['01345930134595']:
#            res = ''
#            res += 'CD:\nTSK:  CF\nLIR:  I\n'
#            ccf = '{:0>13}'.format(random.randint(1,10e12))
#            res += 'CCF:  ICCD_CF_{}\n'.format(ccf)
#            res += 'ESC:  S19\n'
#            res += 'GE:\nGEL:  localizzazione fisica\n'
#            res += 'GET:  georeferenziazione areale\n'
#            res += 'GEP:  WGS84\n'
#            for xyz in feature['geometry']['coordinates']:
#                for x, y, z in xyz:
#                    res += 'GEC:\n'
#                    res += 'GECX: {}\n'.format(x)
#                    res += 'GECY: {}\n'.format(y)
#            res += 'GPT:  rilievo tramite punti di appoggio fiduciali o trigonometrici\nGPM:  posizionamento esatto\n'
#            res += '''AD:
#ADS:
#ADSP: 1
#ADSM: scheda contenente dati liberamente accessibili
#CM:
#CMP:
#CMPD: 2016
#CMPN: Costa, Stefano
#FUR:  Barbaro, Barbara'''
#            print(res)
#        else:
#            pass
