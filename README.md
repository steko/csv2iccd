# csv2iccd

Questo repository contiene una serie di script Python per l'elaborazione di dati
CSV in tabelle semplificate destinate alla conversione nel formato di trasferimento
dell'Istituto Centrale per il Catalogo e la Documentazione (ICCD).

Dalla versione SIGECweb 1.7.0 il vecchio formato ASCII (TRC) non è più
supportato. Importazione ed esportazione dei dati catalografici sono possibili
solo utilizzando il formato ICCD XML-Standard.

La base di partenza è costituita nel caso più semplice da tabelle CSV in cui
ogni colonna corrisponde ad un campo dello standard catalografico e ogni
riga corrisponde a una scheda. Questo caso elementare non gestisce i campi
ripetitivi.

## Come utilizzare lo script `csv2iccd`

Partendo da un file CSV opportunamente strutturato, eseguire:

    python3 csv2iccd.py --esc S236 --tsk MINP --ver 2.00 data/MINP.csv

dove `S236` è il codice della Soprintendenza o altro _ente schedatore_ che verrà
inserito nel tracciato, `MINP` è il tipo di scheda/authority file in base al quale
verrà strutturato l'output, `2.00` è la versione della normativa a cui l'output
si attiene e infine `data/MINP.csv` è il file CSV.

Il risultato è salvato nella directory di esecuzione come pacchetto di importazione
già pronto per essere caricato nell'ambiente SIGECWeb ed è un file zip con nome
generato automaticamente tipo `MINP_2.00_S236_1566813137.zip`.

⚠️ **ATTENZIONE! Al momento lo script gestisce unicamente la normativa MINP 2.00.**

## Come utilizzare lo script `shp2iccd`

Partendo da uno Shapefile, che per comodità dovrebbe contenere una sola _feature_
poligonale, lo script viene eseguito in questo modo:

     python3 shp2iccd.py AreneCandide.shp > AreneCandide.iccd

che molto semplicemente salva l'output testuale in un file per la successiva
elaborazione del pacchetto di importazione.

Lo script dipende dalla libreria [Fiona](https://github.com/Toblerity/Fiona) per la
lettura dello Shapefile.

⚠️ **ATTENZIONE! Al momento lo script non è compatibile con il formato XML-Standard
ICCD.**
